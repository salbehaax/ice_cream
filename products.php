<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);
?>

<?php

/**
 * Liste des produits en vente chez Nice Creams.
 * - les poids sont exprimés en kilogrammes.
 * - les prix sont exprimés en euros (€).
 */

$products = [

    [
        'title' => 'Sorbet Artisanal Abricot',
        'description' => "Toute la douceur et la saveur de l’abricot frais et mûr à souhait. Un sorbet lisse et fort en goût : de l’abricot, du sucre et de l’eau et… un petit secret de fabrication que nous gardons…secret. le demi litre est idéal pour 4 gourmands.",
        'weight' => '0.370',
        'price' => '12.50',
        'image' => 'sorbet-abricot.jpg'
    ],
    [
        'title' => 'Sorbet Artisanal Fraise - Feuille de menthe',
        'description' => "Merveilleuse association d’un sorbet à la fraise relevé de feuilles de menthes fraîches découpée à la main. Fraîcheur et texture garanties pour cette réalisation parfaite et très estivale.",
        'weight' => '0.370',
        'price' => '14.50',
        'image' => 'sorbet-fraise-feuille-de-menthe.jpg'
    ],
    [
        'title' => 'Sorbet Artisanal Litchi',
        'description' => "Subtil sorbet riche en litchi. Finesse et texture sont au rendez-vous.",
        'weight' => '0.370 ',
        'price' => '16.50',
        'image' => 'sorbet-lychee.jpg'
    ],
    [
        'title' => 'Sorbet Artisanal Mangue Alfonso',
        'description' => "Sorbet à la Mangue Alfonso sélectionnées par notre laboratoire. Plus de 64% de fruit !",
        'weight' => '0.370 ',
        'price' => '14.50',
        'image' => 'sorbet-mangue-alfonso.jpg'
    ],
    [
        'title' => 'Glace Artisanale Caramel Beurre Salé',
        'description' => "Glace Caramel au Beurre Salé : réalisée avec du caramel fait maison et une pointe de fleur de sel !",
        'weight' => '0.370 ',
        'price' => '15.50',
        'image' => 'glace-caramel-beurre-sale.jpg'
    ],
    [
        'title' => 'Sorbet Artisanal Pêche de Vigne',
        'description' => "Sorbet à la pêche de vigne : le parfum de la Provence dans votre sorbet. Plus de 64% de fruits !",
        'weight' => '0.370 ',
        'price' => '17.50',
        'image' => 'sorbet-peche-de-vigne.jpg'
    ],
    [
        'title' => 'Glace Artisanale Café Expresso',
        'description' => "Un café Expresso mais … glacé. Idéal pour finir un dîner ou une réception. Idéal aussi pour réaliser un Café liégeois au top.",
        'weight' => '0.370 ',
        'price' => '11.50',
        'image' => 'glace-cafe-expresso.jpg'
    ],
    [
        'title' => 'Sorbet Artisanal Banane',
        'description' => "Sorbet à la banane : 59 % de bananes fraîches ! C’est tout dire...",
        'weight' => '0.370',
        'price' => '12.50',
        'image' => 'sorbet-banane.jpg'
    ],
    [
        'title' => 'Glace Artisanale Réglisse',
        'description' => "Glace à la réglisse, Une couleur profonde et un goût infiniment subtil et une grande longueur en bouche. A essayer, même les plus dubitatifs en redemandent !",
        'weight' => '0.370',
        'price' => '13.50',
        'image' => 'glace-reglisse.jpg'
    ],
    [
        'title' => 'Sorbet Artisanal Noix de Coco',
        'description' => "Sorbet noix de coco : de la noix de coco sous forme de pulpe pour la texture et du lait de coco pour la douceur.",
        'weight' => '0.370',
        'price' => '13.50',
        'image' => 'sorbet-noix-de-coco.jpg'
    ],
];

?>






